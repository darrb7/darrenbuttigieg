﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Deathmenu : MonoBehaviour {

	// Use this for initialization
	public string Menu;

	public void RestartGame()
	{
		FindObjectOfType<gameManager> ().Reset();
	}

	public void QuitToMain()
	{
		SceneManager.LoadScene (0);
	}
}