﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coincollector : MonoBehaviour {


	public int coinstoGive;


	private scoreManager thescoreManager;

	// Use this for initialization
	void Start () {
		thescoreManager = FindObjectOfType<scoreManager> ();

		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	//Built in function when something enters our trigger
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.name == "player") 
		{
			thescoreManager.scoreAdd (coinstoGive);
			gameObject.SetActive (false);
		}
		
	}

}
