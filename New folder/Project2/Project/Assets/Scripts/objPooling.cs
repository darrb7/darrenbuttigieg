﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class objPooling : MonoBehaviour {

	public GameObject pooledObject;

	public int pooledAmount;

	List<GameObject> pooled;



	// Use this for initialization
	void Start () {
		pooled = new List<GameObject> ();

		for (int i = 0; i < pooledAmount; i++)
		{
			GameObject obj = (GameObject)Instantiate (pooledObject);
			obj.SetActive (false);
			pooled.Add (obj);
		}

	}

	//Looking for a Gameobject
	public GameObject GetpooledObject ()
	{
		for (int i = 0; i < pooled.Count; i++)
		{
			//is it active within the scene
			if (!pooled [i].activeInHierarchy)
			{
				return pooled [i];
			}
		}

		GameObject obj = (GameObject)Instantiate (pooledObject);
		obj.SetActive (false);
		pooled.Add (obj);
		return obj;
	}
}
