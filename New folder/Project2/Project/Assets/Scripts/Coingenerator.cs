﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coingenerator : MonoBehaviour {

	public objPooling coinPool;

	public float distancebetweenCoin;

	public void spawnCoin(Vector3 startPosition)
	{
		GameObject coin1 = coinPool.GetpooledObject ();
		coin1.transform.position = startPosition;
		coin1.SetActive (true);

		GameObject coin2 = coinPool.GetpooledObject ();
		coin2.transform.position = new Vector3(startPosition.x - distancebetweenCoin,startPosition.y,startPosition.z);
		coin2.SetActive (true);


		GameObject coin3 = coinPool.GetpooledObject ();
		coin3.transform.position = new Vector3(startPosition.x + distancebetweenCoin,startPosition.y,startPosition.z);
		coin3.SetActive (true);
	}
}
