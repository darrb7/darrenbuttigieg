﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scoreManager : MonoBehaviour {

	public Text scoreText;
	public Text highscore;

	public float scoreCounter;
	public float hiscoreCounter;
	 
	public float pointperSecond;

	public bool increaseScore;

	// Use this for initialization
	void Start () {
		// If it has a value with the entered value, In the PlayerPrefs
		if (PlayerPrefs.HasKey("HighScore"))
		{
			hiscoreCounter = PlayerPrefs.GetFloat ("HighScore");
		}
	}

	// Update is called once per frame
	void Update () {

		if (increaseScore) 
		{
			scoreCounter += pointperSecond * Time.deltaTime;
		}

		if (scoreCounter > hiscoreCounter) 
		{
			hiscoreCounter = scoreCounter;
			PlayerPrefs.SetFloat ("HighScore", hiscoreCounter);
		}

		scoreText.text = "Score: " + Mathf.Round(scoreCounter);
		highscore.text = "Highscore: " + Mathf.Round(hiscoreCounter);
	}

	public void scoreAdd(int pointstoAdd)
	{
		scoreCounter += pointstoAdd;
	}
}
