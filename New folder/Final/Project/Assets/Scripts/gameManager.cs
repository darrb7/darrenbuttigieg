﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameManager : MonoBehaviour {

	public Transform Platformgenerator;
	private Vector3 platformStartPoint;

	public Playermovement thePlayer;
	private Vector3 playerStartPoint;

	private platformDestroyer[] platformList;

	private scoreManager theScoremanager;




	public Deathmenu theDeath;







	// Use this for initialization
	void Start () {
		platformStartPoint = Platformgenerator.position;
		playerStartPoint = thePlayer.transform.position;

		theScoremanager = FindObjectOfType<scoreManager> ();




	}
		
	
	// Update is called once per frame
	void Update () {
		
	}

	public void RestartGame()
	{

		theScoremanager.increaseScore = false;
		thePlayer.gameObject.SetActive (false);
		theDeath.gameObject.SetActive (true);
		//Method that runs byitself, adding timedelay
		//StartCoroutine("RestartGameCo");
	}

	public void Reset()
	{
		theDeath.gameObject.SetActive (false);
		platformList = FindObjectsOfType<platformDestroyer> ();
		for (int i = 0; i < platformList.Length; i++)
		{
			platformList [i].gameObject.SetActive (false);
		}
		thePlayer.transform.position = playerStartPoint;
		Platformgenerator.position = playerStartPoint;
		thePlayer.gameObject.SetActive (true);

		theScoremanager.scoreCounter = 0;
		theScoremanager.increaseScore = true;
	}

	/*public IEnumerator RestartGameCo()
	{
		theScoremanager.increaseScore = false;
		thePlayer.gameObject.SetActive (false);
		yield return new WaitForSeconds (0.5f);
		platformList = FindObjectsOfType<platformDestroyer> ();
		for (int i = 0; i < platformList.Length; i++)
		{
			platformList [i].gameObject.SetActive (false);
		}
		thePlayer.transform.position = playerStartPoint;
		Platformgenerator.position = playerStartPoint;
		thePlayer.gameObject.SetActive (true);

		theScoremanager.scoreCounter = 0;
		theScoremanager.increaseScore = true;
	}*/
}
