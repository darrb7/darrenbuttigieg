﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupManager : MonoBehaviour {

	private bool doublePoints;
	private bool safeMode;

	private bool powerupActive;

	private float powerupCounter;

	private scoreManager theScoreManager;
	private Platformgenerator thePlatformGenerator;

	private float normalPoints;
	private float spikeRate;

	// Use this for initialization
	void Start () {
		theScoreManager = FindObjectOfType<scoreManager> ();
		thePlatformGenerator = FindObjectOfType<Platformgenerator> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (powerupActive) 
		{
			powerupCounter -= Time.deltaTime;

			if (doublePoints)
			{
				theScoreManager.pointperSecond = normalPoints * 2;
			}

			if (powerupCounter <= 0) 
			{
				theScoreManager.pointperSecond = normalPoints;
				powerupActive = false;
			}
		}
	}

	public void ActivatePowerup(bool points,bool safe,float time)
	{
		doublePoints = points; 
		safeMode = safe;
		powerupCounter = time;

		normalPoints = theScoreManager.pointperSecond;

		powerupActive = true;
	}
}
