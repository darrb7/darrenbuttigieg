﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUps : MonoBehaviour {

	public bool doublePoints;
	public bool safeMode;

	public float powerupTime;
	private PowerupManager thePowerupManager;

	// Use this for initialization
	void Start () {
		thePowerupManager = FindObjectOfType<PowerupManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void onCollisionEnter2D(Collider2D other)
	{
		if (other.name == "player")
		{
			thePowerupManager.ActivatePowerup (doublePoints, safeMode, powerupTime);
		}
		gameObject.SetActive (false);
	}
}
