﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Sound : MonoBehaviour {

	public AudioSource MusicSource;
	public AudioClip MusicClip;
	public static bool notPlaying = true;

	// Use this for initialization
	void Start () {
		sound ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Audio.mute) {
			MusicSource.Pause ();

			notPlaying = true;
		}else{
			sound();
	}

	}

		public void sound (){
			MusicSource.clip= MusicClip;
			if(notPlaying){
				MusicSource.Play();
				notPlaying=false;
             }
		}
}

