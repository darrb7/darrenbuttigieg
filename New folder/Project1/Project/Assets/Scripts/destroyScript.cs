﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destroyScript : MonoBehaviour {

	void onTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			Debug.Break ();
		}

		if (other.gameObject.transform.parent)
		//If the object has a parent, destroy the parent, if the object has no parent
 {		//dont destroy the parent
			Destroy (other.gameObject.transform.parent.gameObject);

		} else
		{

			Destroy (other.gameObject);

		}
	}
}
