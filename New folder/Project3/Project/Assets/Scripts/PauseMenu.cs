﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

	public string Menu;

	public GameObject Pause;

	public void PauseGame()
	{
		Time.timeScale = 0f;
		Pause.SetActive (true);
	}

	public void unPauseGame()
	{
		Time.timeScale = 1f;
		Pause.SetActive (false);
	}

	public void RestartGame()
	{
		Time.timeScale = 1f;
		FindObjectOfType<gameManager> ().Reset();
		Pause.SetActive (false);
	}

	public void QuitToMain()
	{
		Time.timeScale = 1f;
		SceneManager.LoadScene (0);
	}
}
