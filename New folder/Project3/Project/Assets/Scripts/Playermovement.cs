﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playermovement : MonoBehaviour {

	public float speed = 1.5f;
	public float Jumpspeed=10f;
	public gameManager theGameManager;

	public AudioSource jumpsound;

	public AudioSource deathsound;

	void FixedUpdate ()
	{
		if (Input.GetKey(KeyCode.LeftArrow))
		{
			transform.position += Vector3.left * speed * Time.deltaTime;
		}
		if (Input.GetKey(KeyCode.RightArrow))
		{
			transform.position += Vector3.right * speed * Time.deltaTime;
		}
		if (Input.GetKey(KeyCode.UpArrow))
		{
			transform.position += Vector3.up * Jumpspeed * Time.deltaTime;
			jumpsound.Play ();
		}

	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "Killbox")
		{
			theGameManager.RestartGame ();
			deathsound.Play ();
		}
	}
}