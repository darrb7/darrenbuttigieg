﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class platformDestroyer : MonoBehaviour {
	public GameObject Destroyer;

	// Use this for initialization
	void Start () {
		Destroyer = GameObject.Find ("Destroyer");
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x < Destroyer.transform.position.x)
		{
			//Destroy (gameObject);
			//Instead of deleting the gameobject disable it
			gameObject.SetActive(false);
		}
			
	}
 }


