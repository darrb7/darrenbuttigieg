﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platformgenerator : MonoBehaviour {
	
	public GameObject thePlatform;
	public GameObject theCoin;
	public Transform generationPoint;
	public float distanceBetween;

	private float platformWidth;

	public float distancebetweenMin;
	public float distancebetweeMax;

	//public GameObject[] Platforms;
	private int platformSelect;
	private float[] platformWidths;

	public objPooling[] theObjectpools;

	private float minHeight;
	public Transform maxHeightPoint;
	private float maxheight;

	public float maxHeightReach;
	private float heightChange;

	private Coingenerator theCoingenerator;
	public float randomSpikes;
	public objPooling spikePool;




	// Use this for initialization
	void Start () {
		//platformWidth = thePlatform.GetComponent<BoxCollider2D> ().size.x;

		platformWidths= new float[theObjectpools.Length];
		for(int i=0; i< theObjectpools.Length; i++)
		{
			platformWidths[i]=  theObjectpools[i].pooledObject.GetComponent<BoxCollider2D> ().size.x;

		}
		minHeight = transform.position.y;
		maxheight = maxHeightPoint.position.y;

		theCoingenerator = FindObjectOfType<Coingenerator> ();

	} 
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x <generationPoint.position.x)
		{
			distanceBetween = Random.Range (distancebetweenMin, distancebetweeMax);

			platformSelect = Random.Range (0, theObjectpools.Length);

			heightChange = transform.position.y + Random.Range (maxHeightReach, -maxHeightReach);

			if (heightChange > maxheight) 
			{
				heightChange = maxheight;
	
			}else if(heightChange < minHeight)
			{

				heightChange = minHeight;
			}
				
			transform.position=new Vector3(transform.position.x + (platformWidths[platformSelect] / 2)+distanceBetween,heightChange,transform.position.z);


			//Instantiate (/*thePlatform*/ Platforms[platformSelect], transform.position, transform.rotation);
			//Knows where to look into the Array, to call a pooled object
			GameObject newPlatform =theObjectpools[platformSelect].GetpooledObject();
			 
			newPlatform.transform.position = transform.position;
			newPlatform.transform.rotation = transform.rotation;
			newPlatform.SetActive (true);





	 

			transform.position=new Vector3(transform.position.x + (platformWidths[platformSelect] / 2),transform.position.y,transform.position.z);
		}

	}
	void Coins(){
		
		transform.position=new Vector3(transform.position.x ,transform.position.y+1f,transform.position.z);
		theCoin.SetActive (false);
	}

}
